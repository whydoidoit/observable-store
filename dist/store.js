"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.raw = raw;
exports.tracked = tracked;
exports.ObservableStore = exports.Store = exports.default = exports.readOnly = void 0;

var _eventemitter = require("eventemitter2");

var _debounce = _interopRequireDefault(require("lodash/debounce"));

var _isFunction = _interopRequireDefault(require("lodash/isFunction"));

var _isObject = _interopRequireDefault(require("lodash/isObject"));

var _react = require("react");

var _reactDom = _interopRequireDefault(require("react-dom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var state = Symbol('state');
var refresh = Symbol('refresh');
var list = Symbol('list');
var must = Symbol('must');
var configure = Symbol('configure');
var resolve = Symbol('resolve');
var watch = Symbol('watch');
var execute = Symbol('execute');
var clean = Symbol('clean');
var recursive = Symbol('recursive');
var idStore = Symbol('id');
var isRaw = Symbol('raw');
var isTracked = Symbol('track');
var readOnly = Symbol('readOnly');
exports.readOnly = readOnly;

function clone(o, id) {
  if (!(0, _isObject.default)(o)) return o;
  id = id || Date.now() + Math.random();
  var newO, i;

  if (_typeof(o) !== 'object') {
    return o;
  }

  if (!o) {
    return o;
  }

  if (o[idStore] === id) {
    return o;
  }

  o[idStore] = id;

  if (Array.isArray(o)) {
    newO = [];

    for (i = 0; i < o.length; i += 1) {
      newO[i] = clone(o[i]);
    }

    return newO;
  }

  newO = {};

  for (i in o) {
    newO[i] = clone(o[i]);
  }

  if (o[isTracked]) newO[isTracked] = true;
  if (o[isRaw]) newO[isRaw] = true;
  return newO;
}

var isRunning = false;
var refreshId = 1;
var ARRAY_MUTATORS = ['copyWithin', 'fill', 'pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'];

function getArgs(func) {
  // First match everything inside the function argument parens.
  var args = func.toString().match(/(?:function\s.*)?\(([^)]*)\)/)[1]; // Split the arguments string into an array comma delimited.

  return args.split(',').map(function (arg) {
    // Ensure no inline comments are parsed and trim the whitespace.
    return arg.replace(/\/\*.*\*\//, '').trim();
  }).filter(function (arg) {
    // Ensure no undefined values are added.
    return arg;
  });
}

function noop() {}

var DUMMY_ARRAY = Object.freeze([]);
var _default = Store;
exports.default = _default;

var Store =
/*#__PURE__*/
function (_EventEmitter) {
  _inherits(Store, _EventEmitter);

  function Store() {
    var _this;

    var initial = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$recursiveObjects = _ref.recursiveObjects,
        recursiveObjects = _ref$recursiveObjects === void 0 ? true : _ref$recursiveObjects,
        _ref$readOnly = _ref.readOnly,
        readOnly = _ref$readOnly === void 0 ? false : _ref$readOnly;

    _classCallCheck(this, Store);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Store).call(this, {
      wildcard: true,
      maxListeners: 0
    }));
    _this[recursive] = recursiveObjects;
    _this[list] = new Set();
    _this[must] = new Set();
    _this[watch] = {};
    _this[state] = {};
    _this[refresh] = (0, _debounce.default)(_this[refresh].bind(_assertThisInitialized(_assertThisInitialized(_this))));

    _this.set(clone(initial));

    _this[refresh].flush();

    _this[readOnly] = readOnly;
    return _this;
  }

  _createClass(Store, [{
    key: refresh,
    value: function value() {
      var _this2 = this;

      var toUpdate = new Set();
      this[list].forEach(function (item) {
        (_this2[watch][item] || DUMMY_ARRAY).forEach(function (watched) {
          toUpdate.add(watched);
        });
      });
      toUpdate.forEach(function (update) {
        Promise.resolve(_this2[update][execute]()).then(function () {}, console.error);
      });
      this[list].forEach(function (item) {
        var value = _this2[state][item];

        _this2[item][resolve].forEach(function (_ref2) {
          var resolve = _ref2.resolve;
          resolve(value);
        });

        _this2.emit("changed.".concat(item), value);
      });
      this[list].clear();
    }
  }, {
    key: configure,
    value: function value(key) {
      var _Object$definePropert,
          _this3 = this;

      if (this[key]) return;
      var self = this;
      var definition = Object.defineProperties((_Object$definePropert = {}, _defineProperty(_Object$definePropert, clean, noop), _defineProperty(_Object$definePropert, resolve, []), _defineProperty(_Object$definePropert, "on", function on(handler) {
        self.on("changed.".concat(key), handler);
        return function () {
          self.off("changed.".concat(key), handler);
        };
      }), _defineProperty(_Object$definePropert, "off", function off(handler) {
        self.off("changed.".concat(key), handler);
      }), _defineProperty(_Object$definePropert, "useValue", function useValue() {
        try {
          var _useState = (0, _react.useState)(0),
              _useState2 = _slicedToArray(_useState, 2),
              forceRefresh = _useState2[1];

          var _useState3 = (0, _react.useState)(self[state][key]),
              _useState4 = _slicedToArray(_useState3, 2),
              result = _useState4[0],
              setResult = _useState4[1];

          (0, _react.useLayoutEffect)(function () {
            function updateValues() {
              var newValue = self[state][key];

              if (window.__debugStore) {
                console.log(key, result, newValue);
              }

              setResult(newValue);
              result = newValue;
              forceRefresh(refreshId++);
              self[must].delete(key);
            }

            var update = function update() {
              if (!window.__testingStore) {
                _reactDom.default.unstable_batchedUpdates(updateValues);
              } else {
                updateValues();
              }
            };

            definition.on(update);
            return function () {
              definition.off(update);
            };
          }, []);
          return result;
        } catch (e) {
          return self[state][key];
        }
      }), _defineProperty(_Object$definePropert, "useChange", function useChange(fn) {
        var db = (0, _debounce.default)(function () {
          _reactDom.default.unstable_batchedUpdates(fn);
        });
        (0, _react.useEffect)(function () {
          db();
          definition.on(db);
          return function () {
            definition.off(db);
            db.cancel();
          };
        }, []);
      }), _defineProperty(_Object$definePropert, "toString", function toString() {
        return (0, _isObject.default)(self[state][key]) ? self[state][key].toString() : self[state][key];
      }), _defineProperty(_Object$definePropert, "valueOf", function valueOf() {
        return (0, _isObject.default)(self[state][key]) ? self[state][key].valueOf() : self[state][key];
      }), _Object$definePropert), {
        value: {
          get: function get() {
            return self[state][key];
          },
          set: function () {
            var _set = _asyncToGenerator(
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee2(value) {
              var fn, args;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!_this3[readOnly]) {
                        _context2.next = 2;
                        break;
                      }

                      throw new Error('Is read only');

                    case 2:
                      if (!(value !== null && (0, _isObject.default)(value) && value.then)) {
                        _context2.next = 6;
                        break;
                      }

                      _context2.next = 5;
                      return value;

                    case 5:
                      value = _context2.sent;

                    case 6:
                      definition[clean]();

                      if (!(value !== null && _this3[recursive] && !(0, _isFunction.default)(value) && (0, _isObject.default)(value) && !Array.isArray(value) && value[isTracked] && !value[isRaw])) {
                        _context2.next = 11;
                        break;
                      }

                      value = new Store(value);
                      _context2.next = 19;
                      break;

                    case 11:
                      if (!(value !== null && (0, _isFunction.default)(value))) {
                        _context2.next = 19;
                        break;
                      }

                      fn = value;
                      args = getArgs(fn);
                      args.forEach(function (arg) {
                        return (self[watch][arg] = self[watch][arg] || []).push(key);
                      });

                      definition[clean] = function () {
                        args.forEach(function (arg) {
                          return self[watch][arg] = self[watch][arg].filter(function (name) {
                            return name !== key;
                          });
                        });
                      };

                      definition[execute] =
                      /*#__PURE__*/
                      _asyncToGenerator(
                      /*#__PURE__*/
                      regeneratorRuntime.mark(function _callee() {
                        var params, result;
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                params = args.map(function (arg) {
                                  return self[state][arg];
                                });
                                _context.prev = 1;
                                result = fn.apply(self, params);

                                if (!result.then) {
                                  _context.next = 9;
                                  break;
                                }

                                _context.next = 6;
                                return result;

                              case 6:
                                _context.t0 = _context.sent;
                                _context.next = 10;
                                break;

                              case 9:
                                _context.t0 = result;

                              case 10:
                                self[state][key] = _context.t0;
                                self[list].add(key);
                                self[refresh]();
                                _context.next = 20;
                                break;

                              case 15:
                                _context.prev = 15;
                                _context.t1 = _context["catch"](1);
                                self[state][key] = '<error>';
                                self[list].add(key);
                                self[refresh]();

                              case 20:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee, this, [[1, 15]]);
                      }));
                      definition[execute]();
                      return _context2.abrupt("return");

                    case 19:
                      // if (value !== null && isEqual(self[state][key], value)) return
                      if (Array.isArray(value) && value[isTracked]) {
                        value = value.map(function (v) {
                          return (0, _isObject.default)(v) && !(0, _isFunction.default)(v) && !v[isRaw] && v[isTracked] ? new Store(v) : v;
                        });
                        ARRAY_MUTATORS.forEach(function (mutator) {
                          var previous = value[mutator].bind(value);

                          value[mutator] = function () {
                            var result = previous.apply(void 0, arguments);
                            value.forEach(function (item, index) {
                              if ((0, _isObject.default)(item) && !(item instanceof Store) && !item[isRaw] && item[isTracked]) {
                                value[index] = new Store(item);
                              }
                            });
                            self[list].add(key);
                            self[must].add(key);
                            self[refresh]();
                            return result;
                          };
                        });
                      }

                      if ((0, _isObject.default)(value) && value !== null) {
                        delete value[isRaw];
                        delete value[isTracked];
                      }

                      if (value !== self[state][key]) {
                        self[state][key] = value;
                        self[list].add(key);
                        self[refresh]();
                      }

                    case 22:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));

            function set(_x) {
              return _set.apply(this, arguments);
            }

            return set;
          }()
        },
        changed: {
          get: function get() {
            return new Promise(function (resolver, reject) {
              definition[resolve].push({
                resolve: resolver,
                reject: reject
              });
            });
          }
        }
      });
      Object.defineProperty(self, key + '$', {
        get: function get() {
          return definition.value;
        },
        set: function set(value) {
          if (_this3[readOnly]) throw new Error('Read only');
          definition.value = value;
        }
      });
      Object.defineProperty(self, '$' + key, {
        get: function get() {
          return definition.useValue();
        },
        set: function set(value) {
          if (_this3[readOnly]) throw new Error('Read only');
          definition.value = value;
        }
      });
      Object.defineProperty(self, key, {
        get: function get() {
          return definition;
        },
        set: function set(value) {
          if (this[readOnly]) throw new Error('Read only');
          definition.value = value;
        }
      });
      return definition;
    }
  }, {
    key: "get",
    value: function get(key) {
      if (!key) {
        return this[state];
      }

      if (!this[state][key]) {
        this.set(_defineProperty({}, key, undefined));
      }

      return this[key];
    }
  }, {
    key: "flush",
    value: function flush() {
      _reactDom.default.flushSync(function () {});

      this[refresh].flush();
    }
  }, {
    key: "set",
    value: function set(update) {
      var _this4 = this;

      if (this[readOnly]) throw new Error('Read only');
      Object.keys(update).forEach(function (key) {
        key = key.replace(/\$/, '');

        _this4[configure](key);

        _this4[key] = update[key];
      }); // this[refresh].flush()
    }
  }]);

  return Store;
}(_eventemitter.EventEmitter2);

exports.ObservableStore = exports.Store = Store;

function raw(object) {
  object[isRaw] = true;
  return object;
}

function tracked(object) {
  object[isTracked] = true;
  return object;
}