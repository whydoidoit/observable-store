import React from "react"
import {readOnly, Store, raw, tracked} from "./store"
import {flushEffects, render} from "react-testing-library"

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    })
}

window.__testingStore = true

describe("Observable Store", function () {
    it("should create a store", function () {
        new Store()
    })
    it("should allow us to set and retrieve a value", function () {
        let store = new Store()
        store.set({a: 1})
        expect(store.a.value).toEqual(1)
        expect(+store.a).toEqual(1)
        expect("" + store.a).toEqual("1")
    })
    it("should immediately set the value", function () {
        let store = new Store({a: 1})
        store.a = 2
        expect(store.a.value).toEqual(2)
    })
    it("should resolve a promise when a value is set", async function () {
        let store = new Store({a: 1})
        setTimeout(() => {
            store.a.value = 2
        })
        await store.a.changed
    })
    it("should allow a primitive once the handler is set", async function () {
        let store = new Store({a: 1})
        setTimeout(() => {
            store.a = 2
        })
        await store.a.changed
    })
    it("should allow us to destructure", function () {
        let store = new Store({a: 1})
        let {a} = store.get()
        expect(a).toEqual(1)
    })
    it("should allow us to have a calculated field", async function () {
        let store = new Store({a: 1, b: 2})
        store.set({c: (a, b) => a + b})
        expect(+store.c).toEqual(3)
    })
    it("should allow us to change a calculated field", async function () {
        let store = new Store({a: 1, b: 2})
        store.set({c: (a, b) => a + b})
        expect(+store.c).toEqual(3)
        store.set({c: (a, b) => a * b})
        expect(+store.c).toEqual(2)
    })
    it("should trap errors in calculated fields", async function () {
        let store = new Store({a: 1, b: 2})
        store.set({
            c: (a, b) => {
                throw new Error("Banana")
            }
        })
        expect(store.c.value).toEqual("<error>")
    })

    it("should allow an async calculation", async function () {
        let store = new Store({a: 1, b: 2})
        store.set({
            c: async (a, b) => {
                await delay(100)
                return a + b
            }
        })
        await store.c.changed
        expect(+store.c).toEqual(3)
    })
    it("should allow hook rendering in react", async function () {
        let store = new Store({a: 1})

        function Component() {
            return <div>Value is {store.$a}</div>
        }

        let {getByText} = render(<Component/>)
        flushEffects()
        getByText("Value is 1")
        store.$a = 2
        store.flush()
        getByText("Value is 2")
    })
    it("should work rendering deep in react", async function () {
        let store = new Store({a: 1})
        let fn = jest.fn()
        function Outer() {
            fn()
            return <Component/>
        }

        function Component() {
            return <div>Value is {store.$a}</div>
        }

        let {getByText} = render(<Outer/>)
        flushEffects()
        expect(store.a.value).toEqual(1)
        getByText("Value is 1")
        store.a = 2
        store.flush()
        getByText("Value is 2")
        expect(fn.mock.calls.length).toEqual(1)
    })

    it("should allow an async calculation to update later", async function () {
        let store = new Store({a: 1, b: 2})
        store.set({
            c: async (a, b) => {
                await delay(100)
                return a + b
            }
        })
        await store.c.changed
        expect(+store.c).toEqual(3)
        setTimeout(() => store.b = 5, 20)
        await store.c.changed
        expect(+store.c).toEqual(6)
    })
    it("should make a store within a store", function () {
        let store = new Store({a: 1, b: tracked({c: 1, d: 2})})
        console.dir(store.b.value)
        expect(store.b.value.c.value).toEqual(1)
    })
    it("should destructure a store within a store", function () {
        let store = new Store({a: 1, b: {c: 1, d: 2}})
        let {c, d} = store.b.value
        expect(+c).toEqual(1)
        expect(+d).toEqual(2)
    })
    it("should destructure a store within a store inside a react render", function () {
        let store = new Store({a: 1, b: {c: 9, d: 2}})

        function Component() {
            const {c} = store.$b
            return <div>Value is {+c}</div>
        }

        let {getByText} = render(<Component/>)
        flushEffects()
        getByText("Value is 9")
    })
    it("should only fire events when the value changes", function () {
        let store = new Store({a: 1})
        let fn = jest.fn()
        let release = store.a.on(fn)
        store.a = 2
        store.flush()
        expect(fn.mock.calls.length).toEqual(1)
        store.a = 2
        store.flush()
        expect(fn.mock.calls.length).toEqual(1)
        store.a = 1
        store.flush()
        expect(fn.mock.calls.length).toEqual(2)
        release()
    })
    it("should be able to be made readonly", function () {
        let store = new Store({a: 1})
        store.a = 2
        store[readOnly] = true
        expect(() => store.a = 3).toThrow("Read only")
        expect(store.a.value).toEqual(2)
        store[readOnly] = false
        expect(() => store.a = 3).not.toThrow("Read only")
        expect(store.a.value).toEqual(3)
        store[readOnly] = true
        expect(() => store.a = 4).toThrow("Read only")
        expect(() => store.set({d: 4})).toThrow("Read only")
    })
    it("should be able to use a $ outside of a render", async function () {
        let store = new Store({a: 1})
        expect(store.$a).toEqual(1)
    })
    it("should be able to store a raw object", function () {
        let store = new Store({a: 1, b: raw({d: 1})})
        expect(store.b.value.d).toEqual(1)
        expect(typeof store.b.value.d).toEqual("number")
    })
    it("should wire the contents of an array", function() {
        let store = new Store({a: 1, b: tracked([tracked({d: 1}), tracked({e: 2})])})
        expect(store.$b[0].$d).toEqual(1)
    })
    it("should rerender and array within a store inside a react render", async function () {
        let store = new Store({a: 1, b: tracked([tracked({c: 9, d: 1}), tracked({c: 10, d: 2})])})

        function Component() {
            console.log("Render")
            return <div>
                {
                    store.$b.map((item, index)=><div key={index}>Value is {item.$c} - {item.$d}</div>)
                }
            </div>
        }

        let {getByText} = render(<Component/>)
        flushEffects()
        getByText("Value is 9 - 1")
        getByText("Value is 10 - 2")
        store.$b.push(tracked({c: 11, d: 4}))
        store.flush()
        getByText("Value is 11 - 4")

    })
    it("should be able to get a variable that isn't defined yet", function() {
        let store = new Store()
        let v = store.get("hello")
        expect(v.useValue).toBeDefined()
    })
    it("should handle null values", function () {
        let store = new Store()
        store.set({menu: tracked({selected: null})})
        expect(store.$menu.$selected).toEqual(null)
    })
    it("should access the raw object when rendering using an _ prefix", function () {
        let store = new Store(tracked({a: 1, b: tracked({c: 9, d: 2})}))

        function Component() {
            const {c} = store.$b
            console.log(c)
            expect(store.$b.c.on).toBeDefined()
            expect(store.$b.$c.on).not.toBeDefined()
            return <div>Value is {c.value}</div>
        }

        let {getByText} = render(<Component/>)
        flushEffects()
        getByText("Value is 9")
    })

})
